import cv2
import numpy as np
import pytesseract
import os
from flask import request,flash,redirect, url_for
from werkzeug.utils import secure_filename
# Importing flask module in the project is mandatory
# An object of Flask class is our WSGI application.
from flask import Flask

# create the Flask app
app = Flask(__name__)

per = 25;


roi = [[(491, 154), (724, 241), 'text', 'signature'],
       [(81, 114), (696, 133), 'text', 'ordre'],
       [(261, 55), (536, 75), 'text', 'amount'],
       [(183, 220), (259, 242), 'text', 'A'],
       [(283, 218), (372, 241), 'text', 'Le'],
       [(395, 211), (460, 240), 'text', 'Bi']]





pytesseract.pytesseract.tesseract_cmd = 'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'


@app.route('/exportcheckdata')
def query_example():
    f = request.files['the_file']
    f.save('./UserChecks/BiatUserModel.jpg')
    print("Image uploaded")
    imgQ = cv2.imread('biat.jpg')
    h, w, c = imgQ.shape

    orb = cv2.ORB_create(1000)
    kp1, des1 = orb.detectAndCompute(imgQ, None)
    # impK1 = cv2.drawKeypoints(imgQ,kp1,None)

    path = 'UserChecks'
    myPicList = os.listdir(path)
    print(myPicList)
    for j, y in enumerate(myPicList):
        img = cv2.imread(path + "/" + y)
        # img = cv2.resize(img, (w // 2 , h //2))
        # cv2.imshow(y, img)
        kp2, des2 = orb.detectAndCompute(img, None)
        bf = cv2.BFMatcher(cv2.NORM_HAMMING)
        matches = bf.match(des2, des1)
        matches.sort(key=lambda x: x.distance)
        good = matches[:int(len(matches) * (per / 100))]
        imgMatch = cv2.drawMatches(img, kp2, imgQ, kp1, good[:100], None, flags=2)
        # imgMatch = cv2.resize(imgMatch, (w // 3 , h //3))
        # cv2.imshow(y, imgMatch)

        srcPoints = np.float32([kp2[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
        dstPoints = np.float32([kp1[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

        M, _ = cv2.findHomography(srcPoints, dstPoints, cv2.RANSAC, 5.0)
        imgScan = cv2.warpPerspective(img, M, (w, h))

        cv2.imshow(y, imgScan)
        imgShow = imgScan.copy()
        imgMask = np.zeros_like(imgShow)

        myData = []

        print(f' ########### Extracting Data from Form {j} #############')

        for x, r in enumerate(roi):

            cv2.rectangle(imgMask, ((r[0][0]), r[0][1]), ((r[1][0]), r[1][1]), (0, 255, 0), cv2.FILLED)
            imgShow = cv2.addWeighted(imgShow, 0.99, imgMask, 0.1, 0)

            imgCrop = imgScan[r[0][1]:r[1][1], r[0][0]:r[1][0]]
            cv2.imshow(str(x), imgCrop)

            if r[2] == 'text':
                print(f' {r[3]} :{pytesseract.image_to_string(imgCrop)}')
                myData.append(pytesseract.image_to_string(imgCrop))

        with open('DataOutput.csv', 'a+') as f:
            for data in myData:
                f.write((str(data) + ','))
            f.write('\n')

        imgShow = cv2.resize(imgShow, (w // 3, h // 3))
        cv2.imshow(y + "2", imgShow)

    # cv2.imshow("KeyPointsQuery",impK1)
    cv2.imshow("Output", imgQ)
    cv2.waitKey(0)

    return 'mrigel'


if __name__ == '__main__':
    # run app in debug mode on port 5000
    app.run(debug=True, port=5000)
